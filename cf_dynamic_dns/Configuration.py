import yaml
from pathlib import Path, PurePath
import os


class ConfigurationFailedException(RuntimeError):
    def __init__(self, error_string):
        self.args += (error_string, )

    def __str__(self):
        return str(self.args[0])


class Configuration:
    """
    Configuration class is a handler to configuration.yml with defined values for run of notificator app.
    """
    def __init__(self, configuration_path: str="./configuration/configuration.yml"):
        working_dir = Path(__file__).resolve().parent
        configuration_path = str(working_dir / PurePath(configuration_path))
        self.configuration_path = configuration_path
        self.known_options = [
            'cf_email',
            'cf_token',
            'record_checkup_timeout',
            'cf_zone_name',
            'record',
        ]
        with open(configuration_path, 'r') as f:
            self.configuration_dict = yaml.load(f, Loader=yaml.FullLoader)
        for k in self.configuration_dict.keys():
            if k not in self.known_options:
                raise ConfigurationFailedException("Key " + str(k) + " not in allowed parameters")

    def __getattr__(self, attr: str):
        """
        This is a method to reference dict attributes from self.configuration
        """
        if attr in self.known_options and attr.upper() in os.environ:
            return os.environ[attr.upper()]
        elif attr in self.configuration_dict:
            return self.configuration_dict[attr]
        raise KeyError(attr + ' not found')
