import time
import logging


def exponential_time_wait_wrapper(f):
    def wrapped(*args, **kwargs):
        i = 10
        while True:
            try:
                return f(*args, **kwargs)
            except Exception as e:
                logging.info("Failed during exponential, waiting")
                time.sleep(i)
                i *= 2
                if i > 60:
                    raise e
    return wrapped
