import CloudFlare
from cf_dynamic_dns.Configuration import Configuration
import logging
import traceback

cf = CloudFlare.CloudFlare(email=Configuration().cf_email, token=Configuration().cf_token)


def lookup_zone_id() -> str:
    try:
        zones = cf.zones.get(params = {'name': Configuration().cf_zone_name, 'per_page': 1})
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        logging.error("Error while fetching zone from CF!")
        traceback.print_exc()
        return ""
    except Exception as e:
        logging.error("Error while fetching zone from CF!")
        traceback.print_exc()
        return ""

    if len(zones) == 0:
        logging.error("No zone from CF!")
        traceback.print_exc()
        return ""

    zone = zones[0]
    return zone['id']


def fetch_record_id():
    zone_id = lookup_zone_id()
    try:
        dns_records = cf.zones.dns_records.get(zone_id)
    except CloudFlare.exceptions.CloudFlareAPIError:
        logging.error("Error while fetching record data from CF!")
        traceback.print_exc()
        return {}
    for record in dns_records:
        if record['type'] == "A" and record['name'] == Configuration().record + "." + Configuration().cf_zone_name:
            return record['id']
    return ""


def fetch_record():
    zone_id = lookup_zone_id()
    try:
        dns_records = cf.zones.dns_records.get(zone_id)
    except CloudFlare.exceptions.CloudFlareAPIError:
        logging.error("Error while fetching record data from CF!")
        traceback.print_exc()
        return {}
    for record in dns_records:
        if record['type'] == "A" and record['name'] == Configuration().record + "." + Configuration().cf_zone_name:
            return {'ip': record['content']}
    return None


def create_record(ip: str):
    zone_id = lookup_zone_id()
    try:
        r = cf.zones.dns_records.post(zone_id, data={
            'name': Configuration().record + "." + Configuration().cf_zone_name, 'type': 'A', 'content': ip
        })
    except CloudFlare.exceptions.CloudFlareAPIError:
        logging.error("Error while pushing record data to CF!")
        traceback.print_exc()


def update_record(ip: str):
    zone_id = lookup_zone_id()
    try:
        r = cf.zones.dns_records.put(zone_id, fetch_record_id(), data={
            'name': Configuration().record + "." + Configuration().cf_zone_name, 'type': 'A', 'content': ip
        })
    except CloudFlare.exceptions.CloudFlareAPIError:
        logging.error("Error while pushing record data to CF!")
        traceback.print_exc()
