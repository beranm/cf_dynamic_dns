from cf_dynamic_dns.Configuration import Configuration
from cf_dynamic_dns.exponential_time_wait_wrapper import exponential_time_wait_wrapper
import time
import requests
import logging
from cf_dynamic_dns.cf import fetch_record, create_record, update_record

logging.basicConfig(level=logging.DEBUG)

while True:
    logging.info("Fetching public IP")
    result = exponential_time_wait_wrapper(requests.get)(
        "https://api.myip.com",
        headers={
            'Content-Type': 'application/json',
        }
    )
    result = result.json()
    current_record = fetch_record()
    if current_record is None:
        logging.info("Record missing gonna submit one")
        create_record(result['ip'])
    elif current_record['ip'] != result['ip']:
        logging.info("Record is different gonna change it")
        update_record(result['ip'])
    else:
        logging.info("Records are OK, doing nothing")

    logging.info("Waiting for another iteration")
    time.sleep(Configuration().record_checkup_timeout)
