# Do you remember DynDNS and how it sucks with those sucky free DNS providers?

This is just a small wrapper which finds out what public IP u have, reads out your credentials from config/env variables and registers your DNS record with that IP.

It is done once in a while and you can use small systemd service deployable to your Turris or any other Pythonable enviroment.

## Configuration option

 * record_checkup_timeout -- time for which the loop should wait until another check of public IP is done
 * cf_token -- cloudflare token
 * cf_email -- cloudflare email
 * cf_zone_name -- cloudflare zone name
 * record -- which record should be adjusted
 
All of those options above can be also used as environment variable in their upper case format, e.g.: CF_TOKEN, ...
